from playhouse.pool import PooledMySQLDatabase

from lib.config import cnf


def Get_Database(name):
    """get pool mysql dataabse."""
    host = cnf.get(name, 'host')
    user = cnf.get(name, 'user')
    password = cnf.get(name, 'password')
    db = cnf.get(name, 'db')

    database = PooledMySQLDatabase(
        db, max_connections=200, stale_timeout=300,
        host=host, user=user, passwd=password
    )
    return database
