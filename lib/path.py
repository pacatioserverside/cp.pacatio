from os.path import dirname
from os.path import abspath
from os.path import join
from os.path import pardir


def Get_Directory():
    current_file    = abspath( __file__ )
    current_dir     = dirname( current_file )
    return current_dir
    
    
def Get_Config():
    current_dir = Get_Directory()
    config_file = join( current_dir , pardir , 'config.cnf' )
    return abspath( config_file )
