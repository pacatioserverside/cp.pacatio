from functools import wraps
from flask import session
from flask import url_for
from flask import redirect

def login_manager(func):
    @wraps(func)
    def wrapper(*args,**kw):
        if session.get('merchant_logged_in'):
            return func(*args,**kw)
        else:
            return redirect( url_for('login') )
    return wrapper
