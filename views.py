from flask import request
from flask import Response
from flask import redirect
from flask import session
from flask import url_for
from flask import render_template
from flask import flash
from flask import jsonify
from flask import abort
from flask import make_response
import datetime


from app import app
from models import *
from lib.auth import login_manager


current_datePlusOne = datetime.datetime.now() + datetime.timedelta(days=1)
minusSeven = datetime.datetime.now() - datetime.timedelta(days=7)
current_datePlusOne = current_datePlusOne.strftime("%Y-%m-%d")
minusSeven = minusSeven.strftime("%Y-%m-%d")


@app.route('/signin', methods=['GET','POST'])
def login():
    if not session.get('merchant_logged_in'):
        if request.method == 'POST':
            email       = request.form.get('email')
            password    = request.form.get('password')
            if email and password:
                try:
                    user = MerchantModel.select().where(
                                MerchantModel.email == email
                                ).get()
                except:
                    return render_template('signin.html',
                                    error = "Email is Invalid!"
                                )
                if user.check_password(password):
                    session['merchant_logged_in']   = True
                    session['merchant_name']        = str(user.name)
                    session['userid']               = str(user.id)
                    session['email']                = str(request.form['email'])
                    return redirect('/')
                else:
                    return render_template('signin.html',
                                    error = "Password is incorrect!"
                                )
        else:
            return render_template('signin.html')
    return redirect('/')


@app.route('/signout')
def logout():
    session.clear()
    return redirect( url_for('login') )



@app.route('/', methods=['GET', 'POST'])
@login_manager
def index():
    current_datePlusOne =   datetime.datetime.now() + datetime.timedelta(days=1)
    minusSeven          =   datetime.datetime.now() - datetime.timedelta(days=30)
    endDate             =   current_datePlusOne.strftime("%Y-%m-%d")
    startdate           =   minusSeven.strftime("%Y-%m-%d")

    merchant_info = CpSummaryTableModel.select().where(
                        CpSummaryTableModel.merchant_id == session['userid']
                        )

    successful_payments = CpSuccessfulPaymentsModel.select().where(
                            CpSuccessfulPaymentsModel.sale_date.between(startdate,endDate),
                            CpSuccessfulPaymentsModel.merchant_id == session['userid']
                            ).group_by(
                                CpSuccessfulPaymentsModel.sale_date
                            )

    successful_payments_total = CpSuccessfulPaymentsModel.select(
                                fn.Sum(
                                    CpSuccessfulPaymentsModel.total_sales
                                ).alias('total')
                            ).where(
                            CpSuccessfulPaymentsModel.sale_date.between(startdate,endDate),
                            CpSuccessfulPaymentsModel.merchant_id == session['userid']
                            )

    concurrent_info = CpConcurrentUsersModel.select().where(
                        CpConcurrentUsersModel.checkin_date.between(startdate, endDate),
                        CpConcurrentUsersModel.merchant_id == session['userid']
                        ).group_by(
                            CpConcurrentUsersModel.checkin_date
                        )

    concurrent_total = CpConcurrentUsersModel.select(
                            fn.Sum(
                                CpConcurrentUsersModel.concurrent_users
                                ).alias('total')
                            ).where(
                            CpConcurrentUsersModel.checkin_date.between(startdate, endDate),
                            CpConcurrentUsersModel.merchant_id == session['userid']
                            )


    new_customer_info = CpNewCustomersModel.select().where(
                            CpNewCustomersModel.new_customer_date.between(startdate, endDate),
                            CpNewCustomersModel.merchant_id == session['userid']
                            ).group_by(
                                CpNewCustomersModel.new_customer_date
                            )

    new_customer_total = CpNewCustomersModel.select(
                                fn.Sum(
                                    CpNewCustomersModel.new_customers
                                ).alias('total')
                            ).where(
                            CpNewCustomersModel.new_customer_date.between(startdate, endDate),
                            CpNewCustomersModel.merchant_id == session['userid']
                            )

    customers_time_info = CpCustomersTimeModel.select().where(
                            CpCustomersTimeModel.merchant_id == session['userid']
                            ).order_by(
                                CpCustomersTimeModel.hour_sort.desc()
                            ).group_by(
                                CpCustomersTimeModel.checkin_hour
                            )

    segment_info        = CpSegmentModel.select().where(
                            CpSegmentModel.merchant_id == session['userid']
                            ).group_by(
                                CpSegmentModel.segment_type
                            )


    endDate     = "/".join( endDate.split('-')[::-1] )
    startdate   = "/".join( startdate.split('-')[::-1] )
    return render_template('index2.html', merchant_name             = session['merchant_name'],
                                            merchant_info             = merchant_info[0],
                                            successful_payments_total = list(successful_payments_total)[0],
                                            new_customer_total        = list(new_customer_total)[0],
                                            concurrent_total          = list(concurrent_total)[0],
                                            concurrent_info           = concurrent_info,
                                            customers_time_info       = customers_time_info,
                                            new_customer              = new_customer_info,
                                            successful_payments       = successful_payments,
                                            segment_info              = segment_info,
                                            dashboard                 = True,
                                            startDate                 = startdate,
                                            endDate                   = endDate,
                                            title = 'home')


@app.route('/report-sales', methods = ['GET', 'POST'])
@app.route('/report-sales/<path:startdate>_<path:endDate>', methods = ['GET', 'POST'])
@login_manager
def report_sales( startdate = minusSeven, endDate = current_datePlusOne ):
    if request.method == 'POST' and request.form['startdate'] and request.form['enddate']:
        startdate   =   request.form['startdate']
        endDate     =   request.form['enddate']


    pmts_day    = PmtsDayModel.select().where(
                                PmtsDayModel.merchant_id == session['userid'],
                                PmtsDayModel.payment_date.between(startdate, endDate)
                                        ).group_by(
                                            PmtsDayModel.payment_date
                                        )
    pmts_day1    = PmtsDayModel.select().where(
                                PmtsDayModel.merchant_id == session['userid'],
                                PmtsDayModel.payment_date.between(startdate, endDate)
                                        ).group_by(
                                            PmtsDayModel.payment_date
                                        )

    return render_template('report-sales2.html',
            pmts_days           = pmts_day,
            another_pmts_days   = pmts_day1,
            title               = 'Reports',
            report_sales_menu   = True,
            report              = True,
            merchant_name       = session['merchant_name'] )

@app.route('/report-order', methods = ['GET', 'POST'])
@app.route('/report-order/<path:startdate>_<path:endDate>', methods = ['GET', 'POST'])
@login_manager
def report_order( startdate = minusSeven, endDate = current_datePlusOne ):
    if request.method == 'POST' and request.form['startdate'] and request.form['enddate']:
        startdate   =   request.form['startdate']
        endDate     =   request.form['enddate']

    pmts_years   = PmtsYearModel.select().where(
                            PmtsYearModel.merchant_id == session['userid']
                                )

    pmts_years1  = PmtsYearModel.select().where(
                            PmtsYearModel.merchant_id == session['userid']
                                )

    return render_template('report-order2.html',
            pmts_years          = pmts_years,
            another_pmts_years  = pmts_years1,
            title               = 'Reports',
            report_order_menu   = True,
            report              = True,
            merchant_name       = session['merchant_name'])


@app.route('/report-wire', methods = ['GET', 'POST'])
@app.route('/report-wire/<path:startdate>_<path:endDate>', methods = ['GET', 'POST'])
@login_manager
def report_wire( startdate = minusSeven, endDate = current_datePlusOne ):
    if request.method == 'POST' and request.form['startdate'] and request.form['enddate']:
        startdate   =   request.form['startdate']
        endDate     =   request.form['enddate']

    wire_details    = CpWireDetailsModel.select().where(
                                CpWireDetailsModel.merchant_id == session['userid'],
                                CpWireDetailsModel.payment_date.between(startdate, endDate)
                                        ).group_by(
                                            CpWireDetailsModel.payment_date
                                        )
    wire_details1    = CpWireDetailsModel.select().where(
                                CpWireDetailsModel.merchant_id == session['userid'],
                                CpWireDetailsModel.payment_date.between(startdate, endDate)
                                        ).group_by(
                                            CpWireDetailsModel.payment_date
                                        )
    return render_template('report-wire.html',
            wire_details            = wire_details,
            another_wire_details    = wire_details1,
            title                   = 'Reports',
            report_wire_menu        = True,
            report                  = True,
            startDate               = startdate,
            endDate                 = endDate,
            merchant_name           = session['merchant_name'])


@app.route('/account/', methods=['GET', 'POST'])
@login_manager
def merchant_admin():
        fname=''
        lname=''
        street1=''
        street2=''
        city=''
        state=''
        zipcode=''
        phone=''
        country=''
        latutude=''
        longitude=''
        lat=''
        lng=''
        mname=''
        query = "SELECT id from categories"
        cats = categories.select()#.query.from_statement(query).all()
        if request.method == 'POST':
            try:
                g = geocoders.GoogleV3()
                latlong=str(request.form['street1']+' '+request.form['city']+' '+request.form['state'])
                place, (lat, lng) = g.geocode(latlong)
                latutude= str(lat)
                longitude=str(lng)
            except:
                lat = ''
                lng = ''

            #print 'lat : %s , \n lng: %s' % (lat,lng)
            info    =   {}
            info['mname']   =   request.form['merchantname']
            info['fname']   =   request.form['firstname']
            info['lname']   =   request.form['lastname']
            info['street1'] =   request.form['street1']
            info['street2'] =   request.form['street2']
            info['city']    =   request.form['city']
            info['state']   =   request.form['state']
            info['country'] =   request.form['country']
            info['phone']   =   request.form['phone']
            info['subcat']  =   request.form['subcat']

            if info['subcat']:
                mdetails = MerchantDetailsModel.update(
                      merchant_name = info['mname'] ,
                      first_name    = info['fname'] ,
                      last_name     = info['lname'],
                      street_1      = info['street1'],
                      street_2      = info['street2'],
                      state         = info['state'],
                      city          = info['city'],
                      phone         = info['phone'],
                      country       = info['country'],
                      #zipcode=request.form['zip'],
                      merchLat      = str(lat),
                      merchLong     = str(lng),
                      subcat_id     = info['subcat']
                    ).where( MerchantDetailsModel.merchant_id == session['userid'])
            else:
                mdetails = MerchantDetailsModel.update(
                      merchant_name = info['mname'] ,
                      first_name    = info['fname'] ,
                      last_name     = info['lname'],
                      street_1      = info['street1'],
                      street_2      = info['street2'],
                      state         = info['state'],
                      city          = info['city'],
                      phone         = info['phone'],
                      country       = info['country'],
                      #zipcode=request.form['zip'],
                      merchLat      = str(lat),
                      merchLong     = str(lng)
                    ).where( MerchantDetailsModel.merchant_id == session['userid'])
            mdetails.execute()
            flash('User Details Updated!')
            return render_template('account-profile2.html',
                                    cats = cats,
                                    info = info,
                                    merchant_name = session['merchant_name']
                                   )
        else:
            try:
                merhcant_details = MerchantDetailsModel.select().where(
                                MerchantDetailsModel.merchant_id == session['userid']
                                ).get()

                info    =   {}
                info['mname']   =   merhcant_details.merchant_name
                info['fname']   =   merhcant_details.first_name
                info['lname']   =   merhcant_details.last_name
                info['street1'] =   merhcant_details.street_1
                info['street2'] =   merhcant_details.street_2
                info['city']    =   merhcant_details.city
                info['state']   =   merhcant_details.state
                info['country'] =   merhcant_details.country
                info['phone']   =   merhcant_details.phone

                #merhcantcheck = Merchant.select().where(Merchant.id==session['userid']).get()
                #status = merhcantcheck.is_approved
            except:
                pass
            return render_template('account-profile2.html',
                                    info = info,
                                    title = "setting",
                                    account = True,
                                    account_menu = True,
                                    merchant_name = session['merchant_name'])
