from flask_peewee.auth import BaseUser
from peewee import *
import datetime

from lib.db import Get_Database


# from app import db

Mysql_database_pacatio = Get_Database('database')

Mysql_database_reports = Get_Database('database_report')


class MysqlModel(Model):
    class Meta:
        database = Mysql_database_pacatio


class MysqlReportsModel(Model):
    class Meta:
        database = Mysql_database_reports


class CpSummaryTableModel(MysqlReportsModel):
    merchant_id = IntegerField()
    total_users = IntegerField()
    new_users = IntegerField()
    sales_this_month = FloatField()
    total_sales = FloatField()
    created_at = DateTimeField(default=datetime.datetime.now)
    updated_at = DateTimeField(default=datetime.datetime.now)

    class Meta:
        db_table = 'cp_summary_table'


class CpConcurrentUsersModel(MysqlReportsModel):
    merchant_id = IntegerField()
    checkin_date = DateField()
    running_total = IntegerField()
    concurrent_users = IntegerField()
    created_at = DateTimeField(default=datetime.datetime.now)
    updated_at = DateTimeField(default=datetime.datetime.now)

    class Meta:
        db_table = 'cp_concurrent_users'


class CpNewCustomersModel(MysqlReportsModel):
    merchant_id = IntegerField()
    new_customer_date = DateField()
    running_total = IntegerField()
    new_customers = IntegerField()
    created_at = DateTimeField(default=datetime.datetime.now)
    updated_at = DateTimeField(default=datetime.datetime.now)

    class Meta:
        db_table = 'cp_new_customers'


class CpSuccessfulPaymentsModel(MysqlReportsModel):
    merchant_id = IntegerField()
    sale_date = DateField()
    successful_payments = IntegerField()
    running_total = FloatField()
    total_sales = FloatField()
    created_at = DateTimeField(default=datetime.datetime.now)
    updated_at = DateTimeField(default=datetime.datetime.now)

    class Meta:
        db_table = 'cp_successful_payments'


class PmtsDayModel(MysqlReportsModel):
    merchant_id = IntegerField()
    payment_date = DateField()
    payment_date_display = CharField()
    visa_cnt = IntegerField()
    mc_cnt = IntegerField()
    amex_cnt = IntegerField()
    total_payments = IntegerField()
    sales_subtotal = FloatField()
    gratuity = FloatField()
    refund_subtotal = FloatField()
    sales_total = FloatField()
    updated_at = DateTimeField(default=datetime.datetime.now)

    class Meta:
        db_table = "pmts_day"


class CpCustomersTimeModel(MysqlReportsModel):
    merchant_id = IntegerField()
    total_users = IntegerField()
    checkin_hour = CharField()
    hour_sort = CharField()
    created_at = DateTimeField(default=datetime.datetime.now)
    updated_at = DateTimeField(default=datetime.datetime.now)

    class Meta:
        db_table = "cp_customers_time"


class CpSegmentModel(MysqlReportsModel):
    merchant_id = IntegerField()
    segment_type = CharField()
    customers = IntegerField()
    created_at = DateTimeField(default=datetime.datetime.now)
    updated_at = DateTimeField(default=datetime.datetime.now)

    class Meta:
        db_table = "cp_segments"


class CpWireDetailsModel(MysqlReportsModel):
    merchant_id = IntegerField()
    payment_date_display = CharField()
    payment_date = DateField()
    total_payments = IntegerField()
    batch_amount = FloatField()
    refund_amount = FloatField()
    deposit_date = DateField()
    platform_fee = FloatField()
    deposit_amount = FloatField()
    wire_date = DateField()
    wire_amount = FloatField()
    updated_on = DateTimeField(default=datetime.datetime.now)

    class Meta:
        db_table = "wire_details"


class PmtsYearModel(MysqlReportsModel):
    merchant_id = IntegerField()
    payment_month = IntegerField()
    payment_month_display = CharField()
    total_payments = IntegerField()
    sales_subtotal = FloatField()
    gratuity = FloatField()
    refund_subtotal = FloatField()
    sales_total = FloatField()
    updated_at = DateTimeField(default=datetime.datetime.now)

    class Meta:
        db_table = "pmts_year"


class MerchantModel(MysqlModel, BaseUser):
    name = CharField()
    password = CharField()
    email = CharField()
    token = CharField()
    apns_token = CharField()
    created_at = DateTimeField(default=datetime.datetime.now)
    active = BooleanField(default=True)
    is_approved = IntegerField(default=0)

    class Meta:
        db_table = 'merchant'


class CategoriesModel(MysqlModel):
    name = CharField()
    is_active = BooleanField(default=True)

    class Meta:
        db_table = 'categories'


class SubCategoriesModel(MysqlModel):
    category_id = ForeignKeyField(
        db_column='category_id', rel_model=CategoriesModel)
    name = CharField()
    is_active = BooleanField(default=True)
    merchLong = CharField()
    recommended = IntegerField(default=0)
    is_active = IntegerField(default=0)

    class Meta:
        db_table = 'subcategories'


class MerchantDetailsModel(MysqlModel):
    merchant_id = CharField()
    merchant_name = CharField()
    first_name = CharField()
    last_name = CharField()
    subcat_id = ForeignKeyField(
        db_column='subcat_id', rel_model=SubCategoriesModel)
    street_1 = CharField()
    street_2 = CharField()
    city = CharField()
    state = CharField()
    zipcode = CharField()
    country = CharField()
    phone = CharField()
    slide_count = CharField()
    created_on = DateTimeField(default=datetime.datetime.now)
    updated_on = DateTimeField(default=datetime.datetime.now)
    merchLat = CharField()
    merchLong = CharField()

    class Meta:
        db_table = 'merchant_details'
