from torndb import Connection

LOCALHOST = "dbm.pacat.io"#107.170.210.126"
DBNAME = "pacatio"
DBNAME_REPORTS = "reports"
DBUSER = "root"
DBPASSWORD = "b3th@Nia"


#connect with DB
def connect_db():
	db = Connection(LOCALHOST,DBNAME, user=DBUSER, password=DBPASSWORD)
	return  db

def connect_db_reports():
	db = Connection(LOCALHOST,DBNAME_REPORTS, user=DBUSER, password=DBPASSWORD)
	return  db

#close the connection from DB
def close_db(db):
	db.close()

